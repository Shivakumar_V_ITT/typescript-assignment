interface account{
    money: Number
    deposit: (value:Number)=>number
}
let bankAccount:account = {
    money: 2000,
    deposit(value:number) {
        return this.money += value;
    }
};

interface accHolder{
    name:String
    hobbies:String[]

}

let myself = {
    name: "Max",
    bankAccount: bankAccount,
    hobbies: ["Sports", "Cooking"]
};

myself.bankAccount.deposit(3000);

console.log(myself);