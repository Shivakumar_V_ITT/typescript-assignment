class add
{
    num1:number;
    num2:number;

    constructor(num1:number,num2:number)
    {
        this.num1=num1
        this.num2=num2
    }

    add():number
    {
        return this.num1+this.num2;
    }
}

var obj = new add(5,6)
var res=obj.add();

console.log("Result:"+res)