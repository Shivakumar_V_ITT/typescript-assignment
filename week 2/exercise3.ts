// Exercise 1
class Car{
    name:String
    acceleration:number

    constructor(name : string)
    {
        this.name=name
        this.acceleration=0
    }
    honk():void{
        console.log("Toooooooooot!");
    };

    accelerate(speed:number):void 
    {
        this.acceleration = this.acceleration + speed;
    }
}
let car = new Car("BMW");
car.honk();
console.log(car.acceleration);
car.accelerate(10);
console.log(car.acceleration);

// Exercise 2

class baseObject{
    width:number =0
    length:number =0

}

class rectangle extends baseObject{
    width=5
    length=2

    calcSize( ):number{
        return this.width * this.length;
    }
}

let obj1:rectangle = new rectangle();
console.log(obj1.calcSize());

// Exercise 3

class Person {
    
    _firstName:string = ""

    public get firstName() {
        return `${this._firstName}`;
    }

    public set firstName(firstName: string) {
        if (firstName.length > 3) 
        {
            this._firstName = firstName;
        }
        else 
        {
                this._firstName = "";
        }
    }
}

let person = new Person();
console.log(person.firstName);
person.firstName = "Ma";
console.log(person.firstName);
person.firstName = "Maximilian";
console.log(person.firstName);