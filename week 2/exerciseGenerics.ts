class MyMap < T extends number | string > {
  private items: object[]

  constructor() {
    this.items = [];
  }

  setItem(key: string, item: T): void {
    let tmp = {};
    tmp[key] = item;

    this.items.push(tmp);
  }

  getItem(key: string): T {
    let ret = null;

    for (let i = 0; i < this.items.length; i++) {
      let curr = this.items[i];

      if (curr[key]) {
        return curr[key];
      }
    }

    return null;
  }

  clear() {
    this.items.length = 0;
  }

  printMap() {
    this.items.forEach((item) => {
      let key: string[] = Object.keys(item);

      console.log('{ "' + key[0] + '": ', item[key[0]], '}');
    });
  }
}

const numberMap = new MyMap<number>();
numberMap.setItem('apples', 5);
numberMap.setItem('bananas', 10);
numberMap.printMap();
 
const stringMap = new MyMap<string>();
stringMap.setItem('name', "Max");
stringMap.setItem('age', "27");
stringMap.printMap();